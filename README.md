# Ansible Role: Systemd Automount Cifs Share

## Description:
This role creates the necessary systemd files to automount a cifs/samba share to a mountpoint(local folder).

## Requirements:
The user must create a credentials file with a the username and password used to authenticate to the cifs server. The domain name is optional.

### Example credentials file:
`username=TestUser`
`password=TestPass123`
`dom=domain` (optional)

## Variables:
| Name | Default Value | Description |
| ---------- | ------------- | -------------- |
| *cifs_credentials_file:* |  /home/user/.credentials  |   Path to the credentials file for authentication to cifs server |
| *cifs_server:*  |          smbserver |                   Name or IP Address of cifs server |
| *cifs_share:*   |          smbshare |                   Name of cifs share on cifs server |
| *cifs_mountpoint:* |  smb  |                     Name of folder to mount cifs share at |
| *cifs_uid:*             |  1000 |                       UID of a user that needs local access to the mounted share |
| *cifs_gid:*             | 1000  |                      GID of a group that needs local access to the mounted share |
